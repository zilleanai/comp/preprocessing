import os
import json
import pickle
import yaml
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
from werkzeug.utils import secure_filename
from zworkflow.preprocessing import get_preprocessing
from zworkflow.config import Config


class PreprocessingController(Controller):

    @route('/preprocessing/<string:project>', methods=['POST'])
    def preprocessing(self, project):
        return jsonify(success=True)

    @route('/save/<string:project>', methods=['POST'])
    def save(self, project):
        os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))
        configfile = os.path.join('workflow.yml') or {}
        with open(configfile) as file:
            config = yaml.load(file, Loader=yaml.SafeLoader)
            if not 'preprocessing' in config:
                config['preprocessing'] = {}
            config['preprocessing']['functions'] = request.json['functions']
            config['preprocessing']['preprocessing_class'] = request.json['preprocessing_class']
            config['preprocessing']['preprocessing_file'] = request.json['preprocessing_file']
        with open(configfile, 'w') as outfile:
            yaml.dump(config,
                      outfile, default_flow_style=False)
        return jsonify(success=True)

    @route('/functions/<string:project>', methods=['GET'])
    def functions(self, project):
        os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))
        configfile = os.path.join('workflow.yml') or {}
        config = Config(configfile)
        preprocessing = get_preprocessing(config)
        functions = list(preprocessing.keys())
        return jsonify(project=project, functions=functions)

    @route('/config/<string:project>', methods=['GET'])
    def config(self, project):
        os.chdir(os.path.join(AppConfig.DATA_FOLDER, project))
        configfile = os.path.join('workflow.yml') or {}
        config = Config(configfile)
        preprocessing_config = config['preprocessing']
        return jsonify(project=project, config=preprocessing_config)

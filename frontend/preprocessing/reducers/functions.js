import { listFunctions } from 'comps/preprocessing/actions'


export const KEY = 'functions'

const initialState = {
  isLoading: false,
  isLoaded: false,
  functionsProjects: [],
  byFunctionsProject: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { functionsProjects, byFunctionsProject } = state
  switch (type) {
    case listFunctions.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listFunctions.SUCCESS:
      if (!functionsProjects.includes(payload.functions.project)) {
        functionsProjects.push(payload.functions.project)
      }
      byFunctionsProject[payload.functions.project] = payload.functions.functions
      return {
        ...state,
        functions: payload.functions.functions,
        isLoaded: true,
      }

    case listFunctions.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listFunctions.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectFunctions = (state) => state[KEY]
export const selectFunctionsList = (state, project) => selectFunctions(state).byFunctionsProject[project]

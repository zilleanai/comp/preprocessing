import { listPreprocessingConfig } from 'comps/preprocessing/actions'


export const KEY = 'LIST_PREPROCESSING_CONFIG'

const initialState = {
  isLoading: false,
  isLoaded: false,
  preprocessingConfigProjects: [],
  byPreprocessingConfigProject: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { preprocessingConfigProjects, byPreprocessingConfigProject } = state
  switch (type) {
    case listPreprocessingConfig.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listPreprocessingConfig.SUCCESS:
      if (!preprocessingConfigProjects.includes(payload.config.project)) {
        preprocessingConfigProjects.push(payload.config.project)
      }
      byPreprocessingConfigProject[payload.config.project] = payload.config.config
      return {
        ...state,
        config: payload.config.config,
        isLoaded: true,
      }

    case listPreprocessingConfig.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listPreprocessingConfig.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectPreprocessingConfig = (state) => state[KEY]
export const selectPreprocessingConfigList = (state, project) => selectPreprocessingConfig(state).byPreprocessingConfigProject[project]

import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { v1 } from 'api'
import Helmet from 'react-helmet'
import { Field, reduxForm, formValues, change } from 'redux-form'
import formActions from 'redux-form/es/actions'
const { reset } = formActions
import { Button, ButtonGroup } from 'react-bootstrap';
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { InfoBox, PageContent } from 'components'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { TextField } from 'components/Form'
import { FunctionList } from 'comps/preprocessing/components'
import { storage } from 'comps/project'
import { save, listPreprocessingConfig } from 'comps/preprocessing/actions'
import { selectPreprocessingConfigList } from 'comps/preprocessing/reducers/preprocessing_config'

const FORM_NAME = 'save'

class Preprocessing extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stepsEnabled: true,
      initialStep: 0,
      steps: [
        {
          element: '.preprocessing-page',
          intro: 'This page allows to configure preprocessing step.',
        },
        {
          element: '.preprocessing-functions',
          intro: 'Functions for preprocessing available in preprocessing.py can be selected or removed.',
        },
        {
          element: '.preprocessing-class',
          intro: 'Classname of preprocessing can be selected here. Should be classname in the preprocessing.py or a default classname.',
        },
        {
          element: '.preprocessing-file',
          intro: 'Preprocessing file which should generally preprocessing.py.',
        },
        {
          element: '.button-primary',
          intro: 'Configuration can be saved here.',
        },
      ],
    }
  }

  renderFunctions = ({ input: { onChange }, project }) => {
    return (<FunctionList project={project} onChange={onChange} />)
  }

  componentWillMount() {
    this.props.listPreprocessingConfig.maybeTrigger({ project: this.props.project })
  }

  componentWillReceiveProps(nextProps) {
    const { listPreprocessingConfig, project, config } = nextProps
    if (project != this.props.project) {
      listPreprocessingConfig.maybeTrigger({ project: project })
    }
    this.setState({
      project,
      config,
      initialValues: config,
    });
  }

  render() {
    const { isLoaded, error, config, handleSubmit, pristine, submitting } = this.props
    const { stepsEnabled, steps, initialStep } = this.state
    if (!isLoaded) {
      return (
        <PageContent className='preprocessing-page'>
          <Helmet>
            <title>Preprocessing</title>
          </Helmet>
          No preprocessing.py file in project.
        </PageContent>
      )
    }
    return (
      <PageContent className='preprocessing-page'>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Preprocessing</title>
        </Helmet>
        <h1>Preprocessing!</h1>

        <form onSubmit={handleSubmit(save)}>
          <div className='preprocessing-functions'>
            <h5>Functions</h5>
            <Field
              name="functions"
              component={this.renderFunctions}
              project={storage.getProject()}
            />
          </div>
          <TextField className='preprocessing-class' name='preprocessing_class' />
          <TextField className='preprocessing-file' name='preprocessing_file' />
          <div className="row">
            <button type="submit"
              className="button-primary"
              disabled={pristine || submitting}
            >
              {submitting ? 'Saving...' : 'Save'}
            </button>
          </div>
        </form>
      </PageContent>)
  }
}

const withForm = reduxForm({
  form: FORM_NAME,
  onSubmitSuccess: (_, dispatch, props) => {
    //dispatch(reset(FORM_NAME)) // TODO Fix bug, wrong default values
    props.listPreprocessingConfig.trigger({ payload: { project: props.project } })
  }
})

const withConnect = connect(
  (state) => {
    const project = storage.getProject()
    const config = selectPreprocessingConfigList(state, project)
    const isLoaded = !!config
    return {
      project,
      config,
      initialValues: config,
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ save, listPreprocessingConfig }, dispatch),
)


const withReducer2 = injectReducer(require('comps/preprocessing/reducers/preprocessing_config'))

const withSaga = injectSagas(require('comps/preprocessing/sagas/save'))

const withSaga2 = injectSagas(require('comps/preprocessing/sagas/preprocessing_config'))

export default compose(
  withReducer2,
  withSaga,
  withSaga2,
  withConnect,
  withForm,
)(Preprocessing)


import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function preprocessingUri(uri) {
  return v1(`/preprocessing${uri}`)
}

export default class Preprocessing {

  static save({ functions, preprocessing_class, preprocessing_file }) {
    return post(preprocessingUri(`/save/${storage.getProject()}`), {
      functions,
      preprocessing_class, preprocessing_file
    })
  }

  static listFunctions({ project }) {
    return get(preprocessingUri(`/functions/${project}`))
  }

  static listPreprocessingConfig({ project }) {
    return get(preprocessingUri(`/config/${project}`))
  }

}

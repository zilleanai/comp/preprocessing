import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import Select from 'react-select';
import { listFunctions, listPreprocessingConfig } from 'comps/preprocessing/actions'
import { selectFunctionsList } from 'comps/preprocessing/reducers/functions'
import { selectPreprocessingConfigList } from 'comps/preprocessing/reducers/preprocessing_config'
import Checkbox from 'components/Checkbox';
import './function-list.scss'

class FunctionList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      checkedItems: new Map(),
      project: props.project,
      selectedOption: null,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    if (this.props.onChange) {
      this.props.onChange( this.optionsToItems(selectedOption))
    }
  }

  componentWillMount() {
    this.props.listFunctions.maybeTrigger({ project: this.props.project })
    this.props.listPreprocessingConfig.maybeTrigger({ project: this.props.project })
  }

  componentWillReceiveProps(nextProps) {
    const { listFunctions, listPreprocessingConfig, project, functions, config } = nextProps
    if (project != this.props.project) {
      listFunctions.maybeTrigger({ project: project })
      listPreprocessingConfig.maybeTrigger({ project: project })
    }
    if (nextProps.isLoaded) {
      this.setState({
        project,
        functions,
        config,
      });
    }
  }

  selectedToIndices(checkedItems) {
    var ids = []
    checkedItems.forEach(function (value, key) {
      if (value == true) {
        ids.push(key)
      }
    }, checkedItems)
    return ids
  }

  itemsToOptions(items) {
    var options = []
    items.forEach(function (value, key) {
      options.push({ value: value, label: value })
    }, items)
    return options
  }

  optionsToItems(options) {
    var items = []
    options.forEach(function (value, key) {
      items.push(value.value)
    }, options)
    return items
  }

  selectedOptions(selected, options) {
    var selOptions = []
    if (!!selected && !!options) {
      options.forEach(function (value, key) {
        if (selected.indexOf(value.value) > -1) {
          selOptions.push(value)
        }
      }, options)
    }
    return selOptions
  }

  render() {
    const { isLoaded, error, functions, config, project, selected, defaultValue } = this.props

    if (!isLoaded) {
      return (<div>{project}</div>)
    }
    const options = this.itemsToOptions(functions)
    const dvalue = this.selectedOptions(defaultValue || config.functions, this.itemsToOptions(functions))
    return (
      <Select
        value={this.state.selectedOption || dvalue || null}
        onChange={this.handleChange}
        isSearchable={true}
        isClearable={true}
        name="functions"
        options={options}
        isMulti
        className="basic-multi-select"
        classNamePrefix="select"
      />
    )
  }
}

const withReducer = injectReducer(require('comps/preprocessing/reducers/functions'))
const withReducer2 = injectReducer(require('comps/preprocessing/reducers/preprocessing_config'))

const withSaga = injectSagas(require('comps/preprocessing/sagas/functions'))
const withSaga2 = injectSagas(require('comps/preprocessing/sagas/preprocessing_config'))

const withConnect = connect(
  (state, props) => {
    const project = props.project
    const selected = props.selected
    const defaultValue = props.defaultValue
    const functions = selectFunctionsList(state, project)
    const config = selectPreprocessingConfigList(state, project)
    const isLoaded = !!functions
    return {
      project,
      functions,
      config,
      selected,
      defaultValue,
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({ listFunctions, listPreprocessingConfig }, dispatch),
)

export default compose(
  withReducer,
  withReducer2,
  withSaga,
  withSaga2,
  withConnect,
)(FunctionList)

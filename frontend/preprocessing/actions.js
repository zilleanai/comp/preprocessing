import { createRoutine } from 'actions'

export const save = createRoutine('preprocessing/SAVE')
export const listFunctions = createRoutine('preprocessing/LIST_FUNCTIONS')
export const listPreprocessingConfig = createRoutine('preprocessing/LIST_PREPROCESSING_CONFIG')
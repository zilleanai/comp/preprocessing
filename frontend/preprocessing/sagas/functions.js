import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listFunctions } from 'comps/preprocessing/actions'
import PreprocessingApi from 'comps/preprocessing/api'
import { selectFunctions } from 'comps/preprocessing/reducers/functions'

export const KEY = 'functions'


export const maybeListFunctionsSaga = function* (project) {
  const { byFunctionsProject, isLoading } = yield select(selectFunctions)
  const isLoaded = !!byFunctionsProject[project]
  if (!(isLoaded || isLoading)) {
    yield put(listFunctions.trigger(project))
  }
}

export const listFunctionsSaga = createRoutineSaga(
  listFunctions,
  function* successGenerator({ payload: project }) {
    const functions = yield call(PreprocessingApi.listFunctions, project)

    yield put(listFunctions.success({
      functions,
    }))
  },
)

export default () => [
  takeEvery(listFunctions.MAYBE_TRIGGER, maybeListFunctionsSaga),
  takeLatest(listFunctions.TRIGGER, listFunctionsSaga),
]

import { call, put, takeLatest } from 'redux-saga/effects'

import { save } from 'comps/preprocessing/actions'
import { createRoutineFormSaga } from 'sagas'
import PreprocessingApi from 'comps/preprocessing/api'


export const KEY = 'save'

export const saveSaga = createRoutineFormSaga(
  save,
  function* successGenerator(payload) {
    const response = yield call(PreprocessingApi.save, payload)
    yield put(save.success(response))
  }
)

export default () => [
  takeLatest(save.TRIGGER, saveSaga)
]

import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'

import { listPreprocessingConfig } from 'comps/preprocessing/actions'
import PreprocessingApi from 'comps/preprocessing/api'
import { selectPreprocessingConfig } from 'comps/preprocessing/reducers/preprocessing_config'

export const KEY = 'config'


export const maybeListPreprocessingConfigSaga = function* (project) {
  const { byPreprocessingConfigProject, isLoading } = yield select(selectPreprocessingConfig)
  const isLoaded = !!byPreprocessingConfigProject[project]
  if (!(isLoaded || isLoading)) {
    yield put(listPreprocessingConfig.trigger(project))
  }
}

export const listPreprocessingConfigSaga = createRoutineSaga(
  listPreprocessingConfig,
  function* successGenerator({ payload: project }) {
    const config = yield call(PreprocessingApi.listPreprocessingConfig, project)

    yield put(listPreprocessingConfig.success({
      config,
    }))
  },
)

export default () => [
  takeEvery(listPreprocessingConfig.MAYBE_TRIGGER, maybeListPreprocessingConfigSaga),
  takeLatest(listPreprocessingConfig.TRIGGER, listPreprocessingConfigSaga),
]

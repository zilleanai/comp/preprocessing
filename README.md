# preprocessing

Component for preprocessing.


## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/preprocessing.git
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.preprocessing',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.preprocessing.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  Preprocessing
} from 'comps/preprocessing/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  Preprocessing: 'Preprocessing',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.Preprocessing,
    path: '/preprocessing',
    component: Preprocessing,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.Preprocessing} />
    ...
</div>
```
